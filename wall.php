<?php
class Wall {
  function __construct() {
    $hub_post_typ = get_hub_clients('slug');
    array_push($hub_post_typ, 'post');
    //var_dump($hub_post_typ);
    $this->wall_args = [
      'post_type' => $hub_post_typ,
      'posts_per_page' => 50,
    ];
  }

  public function get_posts() {
    return Timber::get_posts($this->wall_args, 'ThemePost');
  }
}
